<?php 
// Custom Functions

// Remove the admin bar from the front end
//add_filter( 'show_admin_bar', '__return_false' );


// Remove the version number of WP
// Warning - this info is also available in the readme.html file in your root directory - delete this file!
remove_action('wp_head', 'wp_generator');


// Obscure login screen error messages
function wpfme_login_obscure(){ return '<strong>Error</strong>: Either the username or password is incorrect.';}
add_filter( 'login_errors', 'wpfme_login_obscure' );


// Disable the theme / plugin text editor in Admin
define('DISALLOW_FILE_EDIT', true);

// is_tree()
function is_tree( $pid ) {      // $pid = The ID of the page we're looking for pages underneath
    global $post;               // load details about this page
 
    if ( is_page($pid) )
        return true;            // we're at the page or at a sub page
 
    $anc = get_post_ancestors( $post->ID );
    foreach ( $anc as $ancestor ) {
        if( is_page() && $ancestor == $pid ) {
            return true;
        }
    }
 
    return false;  // we arn't at the page, and the page is not an ancestor
}



    register_nav_menus(array(
        'primary_left' => 'Primary Left',
        'primary_right' => 'Primary Right'
    ));

    function new_excerpt_more( $more ) {
        return '';
    }
add_filter('excerpt_more', 'new_excerpt_more');


    // Register Custom Product Post Type
    function custom_post_type_sheepdog_product() {

        $labels = array(
            'name'                => _x( 'Products', 'Post Type General Name', 'text_domain' ),
            'singular_name'       => _x( 'Product', 'Post Type Singular Name', 'text_domain' ),
            'menu_name'           => __( 'Product', 'text_domain' ),
            'parent_item_colon'   => __( 'Parent Product:', 'text_domain' ),
            'all_items'           => __( 'All Products', 'text_domain' ),
            'view_item'           => __( 'View Product', 'text_domain' ),
            'add_new_item'        => __( 'Add New Product', 'text_domain' ),
            'add_new'             => __( 'New Product', 'text_domain' ),
            'edit_item'           => __( 'Edit Product', 'text_domain' ),
            'update_item'         => __( 'Update Product', 'text_domain' ),
            'search_items'        => __( 'Search products', 'text_domain' ),
            'not_found'           => __( 'No products found', 'text_domain' ),
            'not_found_in_trash'  => __( 'No products found in Trash', 'text_domain' ),
        );
        $args = array(
            'label'               => __( 'product', 'text_domain' ),
            'description'         => __( 'Product information pages', 'text_domain' ),
            'labels'              => $labels,
            'supports'            => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
            'taxonomies'          => array( 'category', 'post_tag' ),
            'hierarchical'        => false,
            'public'              => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'show_in_nav_menus'   => true,
            'show_in_admin_bar'   => true,
            'menu_position'       => 5,
            'menu_icon'           => 'dashicons-cart',
            'can_export'          => true,
            'has_archive'         => true,
            'exclude_from_search' => false,
            'publicly_queryable'  => true,
            'capability_type'     => 'page',
        );
        register_post_type( 'product', $args );

    }

    // Hook into the 'init' action
    add_action( 'init', 'custom_post_type_sheepdog_product', 0 );

    // Register Custom Work Post Type
    function custom_post_type_sheepdog_work() {

        $labels = array(
            'name'                => _x( 'Custom Works', 'Post Type General Name', 'text_domain' ),
            'singular_name'       => _x( 'Custom Work', 'Post Type Singular Name', 'text_domain' ),
            'menu_name'           => __( 'Custom Work', 'text_domain' ),
            'parent_item_colon'   => __( 'Parent Custom Work', 'text_domain' ),
            'all_items'           => __( 'All Custom Works', 'text_domain' ),
            'view_item'           => __( 'View Custom Work', 'text_domain' ),
            'add_new_item'        => __( 'Add New Custom Work', 'text_domain' ),
            'add_new'             => __( 'New Custom Work', 'text_domain' ),
            'edit_item'           => __( 'Edit Custom Work', 'text_domain' ),
            'update_item'         => __( 'Update Custom Work', 'text_domain' ),
            'search_items'        => __( 'Search Custom Works', 'text_domain' ),
            'not_found'           => __( 'No custom works found', 'text_domain' ),
            'not_found_in_trash'  => __( 'No Custom Works found in Trash', 'text_domain' ),
        );
        $rewrite = array(
            'slug'                => 'works',
            'with_front'          => true,
            'pages'               => true,
            'feeds'               => true,
        );
        $args = array(
            'label'               => __( 'custom_work', 'text_domain' ),
            'description'         => __( 'Custom work information pages', 'text_domain' ),
            'labels'              => $labels,
            'supports'            => array( 'title', 'editor' ),
            'taxonomies'          => array( 'category', 'post_tag' ),
            'hierarchical'        => false,
            'public'              => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'show_in_nav_menus'   => true,
            'show_in_admin_bar'   => true,
            'menu_position'       => 5,
            'menu_icon'           => 'dashicons-images-alt2',
            'can_export'          => true,
            'has_archive'         => true,
            'exclude_from_search' => false,
            'publicly_queryable'  => true,
            'rewrite'             => $rewrite,
            'capability_type'     => 'page',
        );
        register_post_type( 'custom_work', $args );

    }

    // Hook into the 'init' action
    add_action( 'init', 'custom_post_type_sheepdog_work', 0 );



    function get_custom_works(){
        $post_id = $_REQUEST{'post_id'};
        $post = get_post($post_id, ARRAY_A, raw);
        $images = get_field('custom_work_image_repeater', $post_id);
        $image_array = array();

        foreach($images as $image){
           $url = $image['custom_work_images']['url'];
           array_push($image_array, $url);
        }
        $default_values = array(
            'title' => $post['post_title'], 
            'content'  => $post['post_content'],
            'images' => $image_array
        );

        $output = json_encode($default_values);
        echo $output;
        die();
    }
    add_action('wp_ajax_nopriv_get_custom_works', 'get_custom_works');
    add_action('wp_ajax_get_custom_works', 'get_custom_works');


?>