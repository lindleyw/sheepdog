<?php while (have_posts()) : the_post(); ?>
	<article <?php post_class(); ?>>
		<section id="product-images" class="large-6 small-12 columns">
			<div id="slider" class="flexslider">
				<?php if(get_field('custom_work_image_repeater')): ?>
				<ul class="product-images-flexslider slides">
					
					<?php while(has_sub_field('custom_work_image_repeater')): ?>
							<?php 

								$image = get_sub_field('custom_work_images');
								$image_id = $image['id'];
								$image_attr = wp_get_attachment_image_src( $image_id, $size='large', $icon = false );

							 ?>
						<li>
							<img src="<?php echo $image_attr[0]; ?>"/>
						</li>
					<?php endwhile; ?>
				</ul>
				<p class="navigation-small">&#8592; Use Arrow Keys to Navigate &#8594;</p>
				<?php endif; ?>
			</div>
			<div id="carousel" class="flexslider">
			<?php if(get_field('custom_work_image_repeater')): ?>
				<ul class="navigation-images slides">
				<?php while(has_sub_field('custom_work_image_repeater')): ?>
							<?php 

								$image = get_sub_field('custom_work_images');
								$image_id = $image['id'];
								$image_attr = wp_get_attachment_image_src( $image_id, $size='thumbnail', $icon = false );

							 ?>
						<li>
							<img src="<?php echo $image_attr[0]; ?>"/>
						</li>
					<?php endwhile; ?>
				</ul>
				<?php endif; ?>
			</div>
		</section>
		<section id="content" class="large-6 small-12 columns">
			<header>
			  <h1 class="entry-title"><?php the_title(); ?></h1>
			</header>
			<div class="entry-content">
			  <?php the_content(); ?>
			</div>
		</section>
	    

	</article>

<?php endwhile; ?>