<footer class="content-info" role="contentinfo">
  <div class="container row">

    <div class="large-3 large-offset-9 columns footer tag">
    	<p>&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?></p>
    </div>
  </div>
</footer>

<?php wp_footer(); ?>
