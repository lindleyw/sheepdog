<div class="header-image"><img src="/assets/img/front_image.jpg" alt=""></div>

<div id="home-page-teasers" class="row">
<?php 
	$args = array(
		'post_type' => 'any',
		'post__in' => array(54, 30, 35)
	);

	$wp_query = new WP_Query($args);

	while($wp_query->have_posts()):
		$wp_query->the_post();
		$id = $wp_query->post->ID;

		$image_src = wp_get_attachment_url( get_post_thumbnail_id($id) ); 

?>
	<div class="teaser large-4 columns">
		<a href="<?php echo get_permalink(); ?>">
			<h2><?php the_title(); ?></h2>
			<div class="image-wrap">
				<img src="<?php echo $image_src ?>" alt="">
			</div>	
		</a>
	</div>
<?php 
	endwhile;

 ?>
</div>
