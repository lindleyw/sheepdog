<header class="banner" role="banner">
  <div class="container">
    
    <nav class="nav-left after-move" role="navigation">
      <?php wp_nav_menu(array('theme_location' => 'primary_left', 'menu_class' => 'nav-left')); ?>
    </nav>
    <a class="brand normal" href="<?php echo home_url(); ?>/">
	    <div class="image-wrap">
	    	<img src="/assets/img/logo.png">
	    </div>
    </a>
    <nav class="nav-right" role="navigation">
      <?php wp_nav_menu(array('theme_location' => 'primary_right', 'menu_class' => 'nav-left')); ?>
    </nav>
  </div>
</header>