<?php while (have_posts()) : the_post(); ?>
	<article <?php post_class(); ?>>
		<div id="product-images" class="large-8 columns large-offset-2">
			<div id="slider" class="flexslider">
				<?php if(get_field('product_image_repeater')): ?>
				<ul class="product-images-flexslider slides">
					
					<?php while(has_sub_field('product_image_repeater')): ?>
							<?php 

								$image = get_sub_field('product_images');
								$image_id = $image['id'];
								$image_attr = wp_get_attachment_image_src( $image_id, $size='large', $icon = false );

							 ?>
						<li>
							<img src="<?php echo $image_attr[0]; ?>"/>
						</li>
					<?php endwhile; ?>
				</ul>

				<?php endif; ?>
			</div>
			<p class="navigation-small">&#8592; Use Arrow Keys to Navigate &#8594;</p>
		</div>
	    <div class="large-12 columns">
	    	<h1 class="entry-title"><?php the_title(); ?></h1>
	    	
	    	<div class="entry-content">
	    	  <?php the_content(); ?>
	    	</div>
	    </div>

	</article>

<?php endwhile; ?>