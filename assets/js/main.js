/* Author: Lindley White

*/

var requestCustomWorks = {

	init: function() {
		this.registerClickHandler();
		this.registerCloseModal();
	},
	registerClickHandler: function() {
		$('#custom-work .custom-product a').on('click', function(e){
			if($(window).width() > 640){
				e.preventDefault();

				$('#custom-work-modal').foundation('reveal', 'open');
				var post_id = $(this).parent().attr('rel');
				doRequest(post_id);
			}
		});

		var url = location.origin + '/wp-admin/admin-ajax.php';

		function doRequest(post_id) {
			$.ajax({
				url: url,
				data: {
					'action' : 'get_custom_works',
					'post_id': post_id
				},
				dataType: 'JSON',
				success: function(data){
					// console.log(data);
					// console.log(data.images);

					$('.spinner').toggle();

					var source = $('#modal-template').html();
					var template = Handlebars.compile(source);

					$('#custom-work-include').html(template(data));
					initSlider($('#slider'), $('#control'));
				},
				error: function(errorThrown){
					//alert('error');
					console.log(errorThrown);
				}

			});
		}
		function initSlider(id){
			$(id).flexslider({
				animation: 'slide',
				controlNav: false,
				animationLoop: true
				//sync: child.selector
			});
		}
	},
	registerCloseModal: function() {
		
			$('body').click(function(){
				if($('#custom-work-modal').hasClass('open')){
					$("#custom-work-modal").foundation('reveal', 'close');
					clearModal();
				}
			});
			$('#custom-work-modal').click(function(e){
				e.stopPropagation();
			});
			$('a.close-reveal-modal').on("click", function(){
				$('#custom-work-modal').foundation('reveal', 'close');
				clearModal();
			});
			$('body').on('keyup', function(e){
				
				if(e.keyCode === 27){
					$('#custom-work-modal').foundation('reveal', 'close');
					clearModal();
				}
			});

			function clearModal() {
				$("#custom-work-include").empty();
				$('.spinner').toggle();
			}
		
	}

};

var responsiveChanges = {
	init: function() {
		this.initResponsive();
	},
	moveHeaderBrandUp: function() {
		$('.banner .container').prepend($('.brand'));
		$('.brand').removeClass('normal').addClass('mobile');
	},
	moveHeaderBrandDown: function() {
		$('.after-move').after($('.brand'));
		$('.brand').removeClass('mobile').addClass('normal');
	},
	initResponsive: function() {
		var wW = $(window).width();
		if(wW < 1040 && $('.brand').hasClass('normal')){
			responsiveChanges.moveHeaderBrandUp();
		}
		if(wW > 1040 && $('.brand').hasClass('mobile')){
			responsiveChanges.moveHeaderBrandDown();
		}
	}
};

$(window).resize(function(){
	var wW = $(window).width();
	if(wW < 1040 && $('.brand').hasClass('normal')){
		responsiveChanges.moveHeaderBrandUp();
	}
	if(wW > 1040 && $('.brand').hasClass('mobile')){
		responsiveChanges.moveHeaderBrandDown();
	}
});

$(document).ready(function(){
	requestCustomWorks.init();
	responsiveChanges.init();

	$('#carousel').flexslider({
	animation: "slide",
		controlNav: false,
		animationLoop: false,
		slideshow: false,
		itemWidth: 40,
		itemMargin: 5,
		asNavFor: '#slider'
	});
	$('#slider').flexslider({
		animation: "slide",
		controlNav: false,
		animationLoop: false,
		slideshow: false,
		sync: "#carousel"
	});
	
});

