<div id="products">
	<div class="content-area">
		<?php 
			$args = array(
				'post_type' => 'product',
				'posts_per_page' => '4',
			);

			$wp_query = new WP_Query($args);

			while($wp_query->have_posts()):
				$wp_query->the_post();
				$id = $wp_query->post->ID;
				//echo $id;
				$image_src = wp_get_attachment_url( get_post_thumbnail_id($id) );
		?>
			<article class="single-product row">
				<div class="image-left large-4 columns"><img src="<?php echo $image_src ?>" alt=""></div>
				<div class="content-right large-8 columns">
					<h2><?php the_title(); ?></h2>
					<p><?php the_excerpt(false); ?></p>

					<a href="<?php echo get_permalink(); ?>">Learn More</a>	
				</div>
				
			</article>

		<?php endwhile; ?>
	</div>
</div>