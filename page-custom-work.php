<section id="custom-work">
	<h1><?php the_title(); ?></h1>

	<div class="content-area">
		<?php while (have_posts()) : the_post(); ?>
		  <?php the_content(); ?>
		<?php endwhile; ?>
		<?php wp_reset_postdata(); ?>
	</div>

	<div class="row">
		<?php 
			$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
			$args = array(
				'post_type' => 'custom_work',
				'posts_per_page' => '6',
				'paged' => $paged
			);

			$the_featured_post = new WP_Query($args); 
			$x = 0;

			while ($the_featured_post->have_posts()):
				
				$the_featured_post->the_post();
				$post = $the_featured_post->post;
				$id = $post->ID;
				$x++;
				if($x%2 == 0){ 
					$case = 'even';
				}else{ 
					$case ='odd';
				}

		?>
			<div class="large-4 small-12 columns custom-product <?php echo $case ?> " rel="<?php echo $id; ?>">
				<a data-reveal-id="custom-work-modal" href="<?php echo get_permalink( $id); ?>"  alt="<?php the_title(); ?>">
				
					<h2><?php the_title() ?></h2>
				<?php if(get_field('custom_work_image_repeater', $id)): ?>
					<?php $image_repeater = get_field('custom_work_image_repeater', $id);
						  $first_image = $image_repeater[0]['custom_work_images'];
						  $sizes = $first_image['sizes'];
						  //print_r($image_repeater); ?>
						  <div class="image-wrap">
						  	<img src="<?php echo $first_image['url']; ?>"  />
						  </div>
				
									

				<?php  endif; ?>
				</a>
			</div>

		<?php  endwhile; ?>
			</div>
			<div class="row">
				<div class="pagination large-2 large-offset-5">
					<?php 
						$big = 999999999; // need an unlikely integer
				
						echo paginate_links( array(
							'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
							'format' => '?paged=%#%',
							'current' => max( 1, get_query_var('paged') ),
							'total' => $the_featured_post->max_num_pages
						) );
					?>
				</div>
			</div>
			<?php wp_reset_postdata(); ?>

</section>
<div id="custom-work-modal" class="reveal-modal" data-reveal>
	<a class="close-reveal-modal">&#215;</a>
	<div class="spinner" style="display:block;">
	  <div class="bounce1"></div>
	  <div class="bounce2"></div>
	  <div class="bounce3"></div>
	</div>
	<div id="custom-work-include">
		
	</div>
</div>

<script id="modal-template" type="text/x-handlebars-template">
	<div class="image-left large-8 columns">
		<div id="slider" class="flexslider">
			<ul class="slides">
			{{#each images}}
				<li><img src="{{this}}" /></li>
			{{/each}}
			</ul>
		</div>
		<p class="navigation-small">&#8592; Use Arrow Keys to Navigate &#8594;</p>
	</div>
	<div class="content-right large-4 columns">
	<h2>{{title}}</h2>
	<p>{{content}}</p>
	</div>

</script>

